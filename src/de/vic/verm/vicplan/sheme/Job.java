package de.vic.verm.vicplan.sheme;

import java.util.Date;

public class Job {
	
	private int projekt_id;
	private int bearbeiter_id;
	private int instrument_id;
	private String dateiname;
	private int coordsystem;
	private int art;
	private String bemerkung;
	private Date datum;
	
	public Job (int projekt_id, int bearbeiter_id, int instrument_id, String dateiname, int coordsystem, int art, String bemerkung, Date datum)
	{
		this.projekt_id=projekt_id;
		this.bearbeiter_id=bearbeiter_id;
		this.instrument_id=instrument_id;
		this.dateiname=dateiname;
		this.coordsystem=coordsystem;
		this.art=art;
		this.bemerkung=bemerkung;
		this.datum=datum;
	}

	public int getProjekt_id() {
		return projekt_id;
	}

	public void setProjekt_id(int projekt_id) {
		this.projekt_id = projekt_id;
	}

	public int getBearbeiter_id() {
		return bearbeiter_id;
	}

	public void setBearbeiter_id(int bearbeiter_id) {
		this.bearbeiter_id = bearbeiter_id;
	}

	public int getInstrument_id() {
		return instrument_id;
	}

	public void setInstrument_id(int instrument_id) {
		this.instrument_id = instrument_id;
	}

	public String getDateiname() {
		return dateiname;
	}

	public void setDateiname(String dateiname) {
		this.dateiname = dateiname;
	}

	public int getCoordsystem() {
		return coordsystem;
	}

	public void setCoordsystem(int coordsystem) {
		this.coordsystem = coordsystem;
	}

	public int getArt() {
		return art;
	}

	public void setArt(int art) {
		this.art = art;
	}

	public String getBemerkung() {
		return bemerkung;
	}

	public void setBemerkung(String bemerkung) {
		this.bemerkung = bemerkung;
	}

	public Date getDatum() {
		return datum;
	}

	public void setDatum(Date datum) {
		this.datum = datum;
	}
	
	
}
