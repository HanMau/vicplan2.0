package de.vic.verm.vicplan.sheme;

import java.util.ArrayList;

public class AttributeDataList {

	private ArrayList<AttributeData> attributes;
	
	
	public AttributeDataList (ArrayList<AttributeData> attributes)
	{
		this.attributes=attributes;
	}
	
	public void addAttribute(AttributeData attribute)
	{
		attributes.add(attribute);
	}
	
	public void deleteAttribute(AttributeData attribute)
	{
		attributes.remove(attribute);
	}
	
	public void deleteAllAttributes(AttributeData attribute)
	{
		attributes.clear();
	}

	public ArrayList<AttributeData> getAttributes() {
		return attributes;
	}

	public void setAttributes(ArrayList<AttributeData> attributes) {
		this.attributes = attributes;
	}
	

}
