package de.vic.verm.vicplan.sheme;

import java.util.ArrayList;

public class Position extends Geometrie {

	private Station bezug;
	private ArrayList<Point> anschluesse;
	private ArrayList<Point> neupunkte;
	
	public Station getBezug() {
		return bezug;
	}
	public void setBezug(Station bezug) {
		this.bezug = bezug;
	}
	public ArrayList<Point> getAnschluesse() {
		return anschluesse;
	}
	public void setAnschluesse(ArrayList<Point> anschluesse) {
		this.anschluesse = anschluesse;
	}
	public ArrayList<Point> getNeupunkte() {
		return neupunkte;
	}
	public void setNeupunkte(ArrayList<Point> neupunkte) {
		this.neupunkte = neupunkte;
	}
	public Position(Station bezug, ArrayList<Point> anschluesse, ArrayList<Point> neupunkte) {
		this.bezug = bezug;
		this.anschluesse = anschluesse;
		this.neupunkte = neupunkte;
	}
	@Override
	protected void graphicRefresh() {
		// TODO Auto-generated method stub
		
	}
	
	
}
