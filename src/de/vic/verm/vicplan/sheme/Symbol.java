package de.vic.verm.vicplan.sheme;

public class Symbol extends Geometrie {

	//required
	private Point bezugspkt;
	private int ad_code;
	
	//optional
	private Point richtungspkt;
	private AttributeDataList nebenattribute;
	
	public Point getBezugspkt() {
		return bezugspkt;
	}

	public int getAd_code() {
		return ad_code;
	}

	public Point getRichtungspkt() {
		return richtungspkt;
	}

	public AttributeDataList getNebenattribute() {
		return nebenattribute;
	}

	private Symbol(SymbolBuilder builder)
	{
		this.bezugspkt=builder.bezugspkt;
		this.ad_code=builder.ad_code;
		this.richtungspkt=builder.richtungspkt;
		this.nebenattribute=builder.nebenattribute;
	}
	
	public static class SymbolBuilder
	{
		//required
		private Point bezugspkt;
		private int ad_code;
		
		//optional
		private Point richtungspkt;
		private AttributeDataList nebenattribute;
		
		public SymbolBuilder(Point bezugspkt, int ad_code)
		{
			this.bezugspkt=bezugspkt;
			this.ad_code=ad_code;
		}
		
		public SymbolBuilder setrichtungspkt(Point richtungspkt)
		{
			this.richtungspkt=richtungspkt;
			return this;
		}
		
		public SymbolBuilder setnebenattribute(AttributeDataList nebenattribute)
		{
			this.nebenattribute=nebenattribute;
			return this;
		}
		
		public Symbol build()
		{
			return new Symbol(this);
		}
	}

	@Override
	protected void graphicRefresh() {
		// TODO Auto-generated method stub
		
	}
	
	
}
