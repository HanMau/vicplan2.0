package de.vic.verm.vicplan.sheme;

import java.util.ArrayList;

public class Line extends Geometrie {
	
	private Point linienstartpkt;
	private Point linienendpkt;
	private ArrayList<LineElement> linienelemente;
	private int ad_code;
	private AttributeDataList nebenattribute;
	
	public Point getLinienstartpkt() {
		return linienstartpkt;
	}
	public void setLinienstartpkt(Point linienstartpkt) {
		this.linienstartpkt = linienstartpkt;
	}
	public Point getLinienendpkt() {
		return linienendpkt;
	}
	public void setLinienendpkt(Point linienendpkt) {
		this.linienendpkt = linienendpkt;
	}
	public ArrayList<LineElement> getLinienelemente() {
		return linienelemente;
	}
	public void setLinienelemente(ArrayList<LineElement> linienelemente) {
		this.linienelemente = linienelemente;
	}
	public int getAd_code() {
		return ad_code;
	}
	public void setAd_code(int ad_code) {
		this.ad_code = ad_code;
	}
	public AttributeDataList getNebenattribute() {
		return nebenattribute;
	}
	public void setNebenattribute(AttributeDataList nebenattribute) {
		this.nebenattribute = nebenattribute;
	}
	
	public Line(Point linienstartpkt, Point linienendpkt, ArrayList<LineElement> linienelemente, int ad_code, AttributeDataList nebenattribute)
	{
		this.linienstartpkt=linienstartpkt;
		this.linienendpkt=linienendpkt;
		this.linienelemente=linienelemente;
		this.ad_code=ad_code;
		this.nebenattribute=nebenattribute;
	}
	@Override
	protected void graphicRefresh() {
		// TODO Auto-generated method stub
		
	}
	
	/*public void schliessen()
	{
		ArrayList<LineElement> liste = this.getLinienelemente();
		int anzahl = liste.size();
		LineElement lastElement = liste.get(anzahl-1);
		int ad_elementcode = lastElement.getAd_elementcode();
		int elementtyp = lastElement.getElementtyp();
		Point punkt = lastElement
		LineElement newElement= new LineElement(linienendpkt, elementtyp, elementtyp)
		
		
	}*/
}
