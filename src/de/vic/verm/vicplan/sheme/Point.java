package de.vic.verm.vicplan.sheme;

public class Point extends Geometrie {

	//required
	private int punkt_id;
	private String art;
	private String herkunft;
	
	//optional
	private double hzri21;
	private double vw22;
	private double schrstr31;
	private int hoehenklasse71;
	private double zielhoe87;
	private double rechtswert;
	private double hochwert;
	private double hoehe;
	private int datzeile;
	private String bemerkung;
	private AttributeDataList nebenattribute;
	
	public int getPunkt_id() {
		return punkt_id;
	}
	public String getArt() {
		return art;
	}
	public String getHerkunft() {
		return herkunft;
	}
	public double getHzri21() {
		return hzri21;
	}
	public double getVw22() {
		return vw22;
	}
	public double getSchrstr31() {
		return schrstr31;
	}
	public int getHoehenklasse71() {
		return hoehenklasse71;
	}
	public double getZielhoe87() {
		return zielhoe87;
	}
	public double getRechtswert() {
		return rechtswert;
	}
	public double getHochwert() {
		return hochwert;
	}
	public double getHoehe() {
		return hoehe;
	}
	public int getDatzeile() {
		return datzeile;
	}
	public String getBemerkung() {
		return bemerkung;
	}
	public AttributeDataList getNebenattribute() {
		return nebenattribute;
	}
	
	private Point(PointBuilder builder)
	{
		this.art=builder.art;
		this.bemerkung=builder.bemerkung;
		this.datzeile=builder.datzeile;
		this.herkunft=builder.herkunft;
		this.hochwert=builder.hochwert;
		this.hoehe=builder.hoehe;
		this.hoehenklasse71=builder.hoehenklasse71;
		this.hzri21=builder.hzri21;
		this.nebenattribute=builder.nebenattribute;
		this.punkt_id=builder.punkt_id;
		this.rechtswert=builder.rechtswert;
		this.schrstr31=builder.schrstr31;
		this.vw22=builder.vw22;
		this.zielhoe87=builder.zielhoe87;
	}
	public static class PointBuilder
	{
		//required
		private int punkt_id;
		private String art;
		private String herkunft;
		
		//optional
		private double hzri21;
		private double vw22;
		private double schrstr31;
		private int hoehenklasse71;
		private double zielhoe87;
		private double rechtswert;
		private double hochwert;
		private double hoehe;
		private int datzeile;
		private String bemerkung;
		private AttributeDataList nebenattribute;
		
		public PointBuilder(int punkt_id, String art, String herkunft)
		{
			this.punkt_id=punkt_id;
			this.art=art;
			this.herkunft=herkunft;
		}
		
		public PointBuilder setHzri21(double hzri21)
		{
			this.hzri21=hzri21;
			return this;
		}
		
		public PointBuilder setVw22(double vw22)
		{
			this.vw22=vw22;
			return this;
		}
		
		public PointBuilder setSchrstr31(double schrstr31)
		{
			this.schrstr31=schrstr31;
			return this;
		}
		
		public PointBuilder setHoehenklasse71(int hoehenklasse71)
		{
			this.hoehenklasse71=hoehenklasse71;
			return this;
		}
		
		public PointBuilder setZielhoe87(double zielhoe87)
		{
			this.zielhoe87=zielhoe87;
			return this;
		}
		
		public PointBuilder setRechtswert(double rechtswert)
		{
			this.rechtswert=rechtswert;
			return this;
		}
		
		public PointBuilder setHochwert(double hochwert)
		{
			this.hochwert=hochwert;
			return this;
		}
		
		public PointBuilder setHoehe(double hoehe)
		{
			this.hoehe=hoehe;
			return this;
		}
		
		public PointBuilder setDatzeile(int datzeile)
		{
			this.datzeile=datzeile;
			return this;
		}
		
		public PointBuilder setNebenattribute(AttributeDataList nebenattribute)
		{
			this.nebenattribute=nebenattribute;
			return this;
		}
		
		public Point build(){
			return new Point(this);
		}
	}
	@Override
	protected void graphicRefresh() {
		// TODO Auto-generated method stub
		
	}
	
	
	
}
