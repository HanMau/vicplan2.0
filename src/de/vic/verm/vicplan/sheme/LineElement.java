package de.vic.verm.vicplan.sheme;

public class LineElement {

	private Point bezugspkt;
	private int ad_elementcode;
	private int elementtyp;
	
	public Point getBezugspkt() {
		return bezugspkt;
	}
	public void setBezugspkt(Point bezugspkt) {
		this.bezugspkt = bezugspkt;
	}
	public int getAd_elementcode() {
		return ad_elementcode;
	}
	public void setAd_elementcode(int ad_elementcode) {
		this.ad_elementcode = ad_elementcode;
	}
	public int getElementtyp() {
		return elementtyp;
	}
	public void setElementtyp(int elementtyp) {
		this.elementtyp = elementtyp;
	}
	
	public LineElement(Point bezugspkt, int ad_elementcode, int elementtyp)
	{
		this.bezugspkt=bezugspkt;
		this.ad_elementcode=ad_elementcode;
		this.elementtyp=elementtyp;
	}
}
