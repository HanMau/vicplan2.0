package de.vic.verm.vicplan.sheme;

public class Text extends Geometrie{
	
	private Point bezugspkt;
	private String text;
	private int ad_code;
	
	public Point getBezugspkt() {
		return bezugspkt;
	}
	public void setBezugspkt(Point bezugspkt) {
		this.bezugspkt = bezugspkt;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public int getAd_code() {
		return ad_code;
	}
	public void setAd_code(int ad_code) {
		this.ad_code = ad_code;
	}
	
	public Text(Point bezugspkt, String text, int ad_code) {
		this.bezugspkt = bezugspkt;
		this.text = text;
		this.ad_code = ad_code;
	}
	
	@Override
	protected void graphicRefresh() {
		// TODO Auto-generated method stub
		
	}
	
	
}
