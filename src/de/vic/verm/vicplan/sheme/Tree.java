package de.vic.verm.vicplan.sheme;


public class Tree extends Geometrie{

	//required
	private Point bezugspkt;
	private int ad_code;
	private int baumart;
	private double stammdurchm;
	private double kronendurchm;
	
	//optional
	private double stammumfang;
	private double stammhoehe;
	private double baumhoehe;
	private double wurzhalsdurchm;
	private double baumscheibenfl;
	private AttributeDataList nebenattribute;
	
	public Point getBezugspkt() {
		return bezugspkt;
	}
	public int getAd_code() {
		return ad_code;
	}
	public int getBaumart() {
		return baumart;
	}
	public double getStammdurchm() {
		return stammdurchm;
	}
	public double getKronendurchm() {
		return kronendurchm;
	}
	public double getStammumfang() {
		return stammumfang;
	}
	public double getStammhoehe() {
		return stammhoehe;
	}
	public double getBaumhoehe() {
		return baumhoehe;
	}
	public double getWurzhalsdurchm() {
		return wurzhalsdurchm;
	}
	public double getBaumscheibenfl() {
		return baumscheibenfl;
	}
	public AttributeDataList getNebenattribute() {
		return nebenattribute;
	}
	
	private Tree(TreeBuilder builder)
	{
		this.ad_code=builder.ad_code;
		this.baumart=builder.baumart;
		this.baumhoehe=builder.baumhoehe;
		this.baumscheibenfl=builder.baumscheibenfl;
		this.bezugspkt=builder.bezugspkt;
		this.kronendurchm=builder.kronendurchm;
		this.nebenattribute=builder.nebenattribute;
		this.stammdurchm=builder.stammdurchm;
		this.stammhoehe=builder.stammhoehe;
		this.stammumfang=builder.stammumfang;
		this.wurzhalsdurchm=builder.wurzhalsdurchm;		
	}
	
	public static class TreeBuilder
	{
		//required
		private Point bezugspkt;
		private int ad_code;
		private int baumart;
		private double stammdurchm;
		private double kronendurchm;
		
		//optional
		private double stammumfang;
		private double stammhoehe;
		private double baumhoehe;
		private double wurzhalsdurchm;
		private double baumscheibenfl;
		private AttributeDataList nebenattribute;
		
		public TreeBuilder(Point bezugspkt, int ad_code, int baumart, double stammdurchm, double kronendurchm)
		{
			this.bezugspkt=bezugspkt;
			this.ad_code=ad_code;
			this.baumart=baumart;
			this.stammdurchm=stammdurchm;
			this.kronendurchm=kronendurchm;
		}
		
		public TreeBuilder setStammumfang(double stammumfang)
		{
			this.stammumfang=stammumfang;
			return this;
		}
		
		public TreeBuilder setStammhoehe(double stammhoehe)
		{
			this.stammhoehe=stammhoehe;
			return this;
		}
		
		public TreeBuilder setBaumhoehe(double baumhoehe)
		{
			this.baumhoehe=baumhoehe;
			return this;
		}
		
		public TreeBuilder setWurzhalsdurchm(double wurzhalsdurchm)
		{
			this.wurzhalsdurchm=wurzhalsdurchm;
			return this;
		}
		
		public TreeBuilder setBaumscheibenfl(double baumscheibenfl)
		{
			this.baumscheibenfl=baumscheibenfl;
			return this;
		}
		
		public TreeBuilder setNebenattribute(AttributeDataList nebenattribute)
		{
			this.nebenattribute=nebenattribute;
			return this;
		}
		
		public Tree build()
		{
			return new Tree(this);
		}
	}

	@Override
	protected void graphicRefresh() {
		// TODO Auto-generated method stub
		
	}
	
	
}