package de.vic.verm.vicplan.sheme;

public class TargetPointSets {

	private int punkt_id;
	private int satz_id;
	private int fernrohrlage;
	private double hzri21;
	private double vw22;
	private double schrstr31;
	private double zielhoe87;
	private int datzeile;
	
	public int getPunkt_id() {
		return punkt_id;
	}
	public void setPunkt_id(int punkt_id) {
		this.punkt_id = punkt_id;
	}
	public int getSatz_id() {
		return satz_id;
	}
	public void setSatz_id(int satz_id) {
		this.satz_id = satz_id;
	}
	public int getFernrohrlage() {
		return fernrohrlage;
	}
	public void setFernrohrlage(int fernrohrlage) {
		this.fernrohrlage = fernrohrlage;
	}
	public double getHzri21() {
		return hzri21;
	}
	public void setHzri21(double hzri21) {
		this.hzri21 = hzri21;
	}
	public double getVw22() {
		return vw22;
	}
	public void setVw22(double vw22) {
		this.vw22 = vw22;
	}
	public double getSchrstr31() {
		return schrstr31;
	}
	public void setSchrstr31(double schrstr31) {
		this.schrstr31 = schrstr31;
	}
	public double getZielhoe87() {
		return zielhoe87;
	}
	public void setZielhoe87(double zielhoe87) {
		this.zielhoe87 = zielhoe87;
	}
	public int getDatzeile() {
		return datzeile;
	}
	public void setDatzeile(int datzeile) {
		this.datzeile = datzeile;
	}
	public TargetPointSets(int punkt_id, int satz_id, int fernrohrlage, double hzri21, double vw22, double schrstr31,
			double zielhoe87, int datzeile) {
		this.punkt_id = punkt_id;
		this.satz_id = satz_id;
		this.fernrohrlage = fernrohrlage;
		this.hzri21 = hzri21;
		this.vw22 = vw22;
		this.schrstr31 = schrstr31;
		this.zielhoe87 = zielhoe87;
		this.datzeile = datzeile;
	}
	
	
}
