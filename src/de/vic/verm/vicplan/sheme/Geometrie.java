package de.vic.verm.vicplan.sheme;

import java.awt.Graphics2D;
import java.awt.Shape;

public abstract class Geometrie {

	protected Shape graphic;
	
	public Shape getGraphic()
	{
		return graphic;
	}
	
	protected abstract void graphicRefresh();
	
	public void draw(Graphics2D g2)
	{
		graphicRefresh();
		g2.draw(graphic);
	}
}
