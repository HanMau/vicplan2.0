package de.vic.verm.vicplan.sheme;

public class AttributeData {

	private String name;
	private String wert;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getWert() {
		return wert;
	}
	public void setWert(String wert) {
		this.wert = wert;
	}
	
	public AttributeData(String name, String wert)
	{
		this.name=name;
		this.wert=wert;
	}
}
