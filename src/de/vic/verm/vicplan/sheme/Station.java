package de.vic.verm.vicplan.sheme;

import java.util.Date;

public class Station extends Geometrie{
	//required
	private Point bezugspkt;
	private double instrhoe;
	private Date datum;

	//optional
	private int datzeile;

	public Point getBezugspkt() {
		return bezugspkt;
	}

	public double getInstrhoe() {
		return instrhoe;
	}
	
	public Date getDatum() {
		return datum;
	}
	
	public int getDatzeile() {
	return datzeile;
	}
	
	
		private Station(StationBuilder builder)
	{
		this.bezugspkt=builder.bezugspkt;
		this.instrhoe=builder.instrhoe;
		this.datum=builder.datum;
		this.datzeile=builder.datzeile;
	}
	
	public static class StationBuilder
	{
		//required
		private Point bezugspkt;
		private double instrhoe;
		private Date datum;
		
		//optional
		private int datzeile;
		
		public StationBuilder(Point bezugspkt, double instrhoe, Date datum)
		{
			this.bezugspkt=bezugspkt;
			this.instrhoe=instrhoe;
			this.datum=datum;
		}
		
		public StationBuilder setdatum(Date datum)
		{
			this.datum=datum;
			return this;
		}
		
		public StationBuilder setdatzeile(int datzeile)
		{
			this.datzeile=datzeile;
			return this;
		}
		
		public Station build()
		{
			return new Station(this);
		}
	}
		@Override
	protected void graphicRefresh() {
		// TODO Auto-generated method stub
		
		}
}
